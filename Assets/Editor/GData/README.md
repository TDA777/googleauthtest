This unity package integrates unity with Google Drive API to access Google Spreadsheets via Auth 2.0 by Aleksandr Petrov

Google “*.dll” at “/GDataDB/plugins/“ directory a taken from Google Data API SDK, tar file for Unix based installations, 2.1.0  - https://code.google.com/p/google-gdata/downloads/detail?name=libgoogle-data-mono-2.1.0.0.tar.gz&can=2&q=

Some resources are partly taken from Unity-GoogleData by Kim, Hyoun Woo

Newtonsoft.Json source code for net 2.0 is taken here https://github.com/JamesNK/Newtonsoft.Json


Integration and access code to Google Spreadsheets is partly taken from official google documentation - https://developers.google.com/google-apps/spreadsheets/